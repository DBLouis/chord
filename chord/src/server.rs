use crate::{node::LockedNode, worker::Worker};
use nuuksio::{future, net::Socket, prelude::*, runtime::Runtime};
use ros::{cell::RefCell, collections::VecDeque, error::Error, prelude::*, rc::Rc};

#[derive(Debug)]
pub struct Server {
    socket: Socket,
    node: Rc<LockedNode>,
}

impl Server {
    pub fn new(socket: Socket, node: Rc<LockedNode>) -> Self {
        Self { socket, node }
    }

    pub async fn run(mut self) {
        let rt = Runtime::get();
        info!("server start");

        let pool = Rc::new(RefCell::new(VecDeque::new()));

        while self.node.is_alive() {
            match self.node.while_alive(self.socket.accept()).await {
                Ok(Ok((socket, saddr))) => {
                    debug!(rt; "connection from {}", saddr);

                    let mut worker = match pool.borrow_mut().pop_front() {
                        Some(worker) => worker,
                        None => {
                            let node = Rc::clone(&self.node);
                            Box::new(Worker::new(node))
                        }
                    };

                    let pool = Rc::clone(&pool);

                    rt.spawn(async move {
                        worker.run(socket).await;
                        // Put back worker into the pool so it can be reused.
                        pool.borrow_mut().push_back(worker);
                    });

                    // Yield to prioritize new worker task.
                    future::yield_now().await;
                }
                Ok(Err(_)) => todo!(),
                Err(Error::Intr) => { /* check alive */ }
                Err(e) => {
                    error!(rt; "{}", e);
                    self.node.kill();
                }
            }
        }

        info!("server stop");
    }
}

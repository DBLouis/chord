use bincode::{Decode, Encode};
use getrandom::getrandom;
use ros::{convert::TryInto, fmt};

/// A 256 bits random seed.
#[derive(Debug, Clone, Copy, PartialEq, Encode, Decode)]
#[repr(transparent)]
pub struct Seed {
    bytes: [u8; 32],
}

impl Seed {
    pub fn random() -> Self {
        let mut bytes = [0; 32];
        getrandom(&mut bytes).unwrap();
        Self { bytes }
    }
}

impl AsRef<[u8; 32]> for Seed {
    fn as_ref(&self) -> &[u8; 32] {
        &self.bytes
    }
}

// Used to initialize PRNG
impl From<Seed> for u128 {
    fn from(seed: Seed) -> u128 {
        u128::from_ne_bytes(seed.bytes[0..16].try_into().unwrap())
    }
}

impl fmt::Display for Seed {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:#032X}{:032X}",
            u128::from_ne_bytes(self.bytes[0..16].try_into().unwrap()),
            u128::from_ne_bytes(self.bytes[16..32].try_into().unwrap())
        )
    }
}

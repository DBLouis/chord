use crate::{
    message::Message,
    net::{Codec, NetAddr, WAIT_DELAY},
    node::{LockedNode, Node},
    peer::Peer,
    ring::Ring,
    scheduler::Scheduler,
    server::Server,
    version::VERSION,
};
use nuuksio::{
    net::Channel,
    prelude::*,
    runtime::Runtime,
    signal::{SignalHandler, SignalSet},
    time::{sleep, timeout},
};
use ros::{
    error::{Error, Result},
    libc,
    prelude::*,
    rc::{Rc, Weak},
    time::Duration,
};

/// How long does a joining node wait before aborting
const JOIN_TIMEOUT: Duration = Duration::from_secs(30);

/// Join an existing Chord ring
pub async fn join(caddr: NetAddr, paddr: NetAddr, entry: NetAddr) -> Result<Rc<LockedNode>> {
    info!("attempting to join with entry node at {}", entry);

    let rt = Runtime::get();

    let node = timeout(JOIN_TIMEOUT, try_join(caddr, paddr, entry)).await??;
    info!("join done");

    let node = Rc::new(LockedNode::new(node));

    let signal_handler = {
        let node = Rc::downgrade(&node);
        let mut set = SignalSet::empty();
        set.add(libc::SIGINT);
        set.add(libc::SIGTERM);
        set.add(libc::SIGHUP);
        SignalHandler::new(
            set,
            Box::new(move |_| {
                if let Some(node) = Weak::upgrade(&node) {
                    rt.spawn(async move {
                        node.leave().await.unwrap();
                    });
                }
            }),
        )
    }?;
    rt.spawn(signal_handler.catch());

    let socket = caddr.bind().await?;
    let server = Server::new(socket, Rc::clone(&node));
    rt.spawn(server.run());

    let scheduler = Scheduler::new(Rc::clone(&node));
    rt.spawn(scheduler.run());

    Ok(node)
}

async fn try_join(caddr: NetAddr, paddr: NetAddr, mut entry: NetAddr) -> Result<Node> {
    loop {
        let codec = Codec::new();
        let socket = entry.connect().await?;
        let mut channel = Channel::new(socket, codec);
        debug!("connected to {}", entry);

        let message = Message::Join { version: VERSION, addr: caddr };
        channel.send(&message).await?;

        let message = channel.recv().await?;

        match message {
            Message::JoinOk { id, predecessor_id, successors } => {
                // After receiving a JoinOk message this node is expected
                // to be ready to process messages ASAP
                let predecessor = Peer { id: predecessor_id, addr: entry };
                let ring = Ring::new(id, predecessor, &successors);
                debug!("{:?}", ring);
                let node = Node::new(caddr, paddr, ring);
                break Ok(node);
            }

            Message::JoinFw { predecessor } => {
                trace!("forward to {}", predecessor);
                entry = predecessor.addr;
            }

            Message::JoinErr => {
                error!("failed to join");
                return Err(Error::ConnectionRefused);
            }

            Message::Pending => {
                sleep(WAIT_DELAY).await?;
            }

            message => {
                // bug impl or malicious: abort join
                error!("unexpected message: {:?}", message);
                return Err(Error::InvalidInput);
            }
        }
    }
}

use bincode::{Decode, Encode};
use const_parse::parse_u128;
use ros::fmt;

/// Compressed 24 bits version number
#[derive(Debug, Copy, Clone, PartialEq, Encode, Decode)]
pub struct Version {
    pub major: u8,
    pub minor: u8,
    pub patch: u8,
}

impl Version {
    pub fn is_compatible(&self) -> bool {
        self.major == VERSION.major
    }
}

pub const VERSION: Version = {
    Version {
        major: parse_u128(env!("CARGO_PKG_VERSION_MAJOR")) as u8,
        minor: parse_u128(env!("CARGO_PKG_VERSION_MINOR")) as u8,
        patch: parse_u128(env!("CARGO_PKG_VERSION_PATCH")) as u8,
    }
};

impl fmt::Display for Version {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "{}.{}.{}", self.major, self.minor, self.patch)
    }
}

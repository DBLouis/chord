use crate::{node::LockedNode, seed::Seed};
use nuuksio::{prelude::*, runtime::Runtime, time::interval_at};
use oorandom::Rand64;
use ros::{
    error::{Error, Result},
    rc::Rc,
    time::{Duration, Instant},
};

#[derive(Debug)]
pub struct Scheduler {
    seed: Seed,
    node: Rc<LockedNode>,
}

impl Scheduler {
    pub fn new(node: Rc<LockedNode>) -> Self {
        let seed = Seed::random();
        Self { seed, node }
    }

    pub async fn run(self) {
        info!("scheduler start");
        match self.try_run().await {
            Ok(_) => info!("scheduler done"),
            Err(e) => warn!("scheduler done: {}", e),
        }
    }

    pub async fn try_run(self) -> Result<()> {
        let rt = Runtime::get();
        let mut rng = Rand64::new(self.seed.into());

        let delay = Duration::from_millis(rng.rand_range(5_000..10_000));
        let start = Instant::now() + delay;
        let period = Duration::from_millis(rng.rand_range(2_500..3_500));

        let mut interval = interval_at(start, period)?;

        debug!(rt; "will stabilize in {}", delay);

        while self.node.is_alive() {
            let future = async {
                interval.tick().await;
                self.node.unlock_exclusive().await
            };

            let mut node = match self.node.while_alive(future).await {
                Err(Error::Intr) => continue,
                Err(e) => {
                    warn!(rt; "{}", e);
                    // TODO should we kill node here?
                    continue;
                }
                Ok(node) => node,
            };

            debug!(rt; "stabilizing now");
            match self.node.while_alive(node.stabilize()).await {
                Err(Error::Intr) => continue,
                Err(e) => {
                    warn!(rt; "{}", e);
                    // TODO should we kill node here?
                    continue;
                }
                Ok(Err(_)) => todo!(),
                Ok(Ok(())) => {}
            }

            // Ensure lock is released
            drop(node);

            debug!(rt; "will stabilize in {} seconds", period);
        }

        Ok(())
    }
}

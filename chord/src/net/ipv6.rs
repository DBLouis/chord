use bincode::{
    de::Decoder,
    enc::Encoder,
    error::{DecodeError, EncodeError},
    Decode, Encode,
};
use blake3::Hasher;
use nuuksio::net::{AnySocketAddr, Family, Ipv6Addr, Ipv6SocketAddr, Protocol, Socket, SocketType};
use ros::{error::Error, fmt};

pub const FAMILY: Family = Family::Ipv6;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct NetAddr {
    inner: Ipv6SocketAddr,
}

impl NetAddr {
    pub async fn bind(&self) -> Result<Socket, Error> {
        Socket::bind(FAMILY, SocketType::Stream, Protocol::Tcp, self.inner.as_ref()).await
    }

    pub async fn connect(&self) -> Result<Socket, Error> {
        Socket::connect(FAMILY, SocketType::Stream, Protocol::Tcp, self.inner.as_ref()).await
    }

    #[allow(clippy::should_implement_trait)]
    pub fn hash(&self, hasher: &mut Hasher) {
        hasher.update(&self.inner.ip().octets());
        hasher.update(&self.inner.port().to_ne_bytes());
    }
}

impl Default for NetAddr {
    fn default() -> Self {
        Self { inner: Ipv6SocketAddr::new(Ipv6Addr::UNSPECIFIED, 0) }
    }
}

impl From<Ipv6SocketAddr> for NetAddr {
    fn from(saddr: Ipv6SocketAddr) -> Self {
        Self { inner: saddr }
    }
}

impl TryFrom<AnySocketAddr> for NetAddr {
    type Error = Error;

    fn try_from(saddr: AnySocketAddr) -> Result<Self, Error> {
        Ok(Self { inner: saddr.try_into()? })
    }
}

impl Encode for NetAddr {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        self.inner.ip().octets().encode(encoder)?;
        self.inner.port().encode(encoder)?;
        Ok(())
    }
}

impl Decode for NetAddr {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        let ip = Ipv6Addr::from(u128::decode(decoder)?);
        let port = u16::decode(decoder)?;
        Ok(Self { inner: Ipv6SocketAddr::new(ip, port) })
    }
}

impl_borrow_decode!(NetAddr);

impl fmt::Debug for NetAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.inner, formatter)
    }
}

impl fmt::Display for NetAddr {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.inner, formatter)
    }
}

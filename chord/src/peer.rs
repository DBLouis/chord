use crate::{id::Id, net::NetAddr};
use bincode::{Decode, Encode};
use ros::fmt;

#[derive(Debug, Copy, Clone, PartialEq, Encode, Decode)]
pub struct Peer {
    pub id: Id,
    pub addr: NetAddr,
}

impl Peer {
    pub(crate) fn dummy(id: Id) -> Self {
        Self { id, addr: NetAddr::default() }
    }

    pub fn new(id: Id, addr: NetAddr) -> Self {
        Self { id, addr }
    }
}

impl fmt::Display for Peer {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "{:#}@{}", self.id, self.addr)
    }
}

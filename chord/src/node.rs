use crate::{
    id::Id,
    message::Message,
    net,
    net::{Codec, NetAddr, WAIT_DELAY},
    peer::Peer,
    ring::Ring,
};
use nuuksio::{
    net::{Channel, Encoder},
    prelude::*,
    sync::{Mutex, OwnedMutexGuard, RwLock, RwLockReadGuard, RwLockWriteGuard, Watch},
    time::timeout,
};
use ros::{
    cell::Cell,
    error::{Error, Result},
    fmt,
    future::Future,
    prelude::*,
    rc::Rc,
};

// TODO we need to make sure the API enforces correct usage of chord Node.
// for ex. application calling `is_owner_of`, then modifying its state based on that.
// it must keep the lock guard alive while doing this transaction, otherwise it could
// modify its state but another node already has taken ownership of it.

/// A Chord Node
pub struct Node {
    codec: Codec,
    ring: Ring,
    // Public address for ring-maintenance protocol
    pub caddr: NetAddr,
    // Public address for application protocol
    pub paddr: NetAddr,
}

impl Node {
    /// Creates a new Node.
    pub fn new(caddr: NetAddr, paddr: NetAddr, ring: Ring) -> Self {
        let codec = Codec::new();
        Self { codec, ring, caddr, paddr }
    }

    pub fn id(&self) -> Id {
        self.ring.id()
    }

    // is node responsible for `id`?
    pub fn is_owner_of(&self, id: Id) -> bool {
        id.between(self.ring.predecessor().id, self.id())
    }

    pub fn predecessor(&self) -> &Peer {
        self.ring.predecessor()
    }

    pub fn successors(&self) -> &[Peer] {
        self.ring.successors()
    }

    pub fn is_predecessor_of(&self, id: Id) -> bool {
        id.between(self.id(), self.ring.successor().id)
    }

    pub fn is_successor_of(&self, id: Id) -> bool {
        id.between(self.ring.predecessor().id, self.id())
    }

    pub fn set_predecessor(&mut self, peer: Peer) {
        self.ring.set_predecessor(peer)
    }

    pub fn push_successor(&mut self, peer: Peer) {
        self.ring.push_successor(peer)
    }

    pub async fn find_successor(&mut self, id: Id) -> Result<Peer> {
        assert!(!self.is_successor_of(id));

        let message = Message::FindSuccessor { id };

        if let Some((peer, message)) = self.ring.try_query_successor(&mut self.codec, &message).await {
            trace!("find successor with {}", peer);
            match message {
                Message::SuccessorIs { peer } => {
                    return Ok(peer);
                }
                Message::SuccessorIsMe => {
                    return Ok(peer);
                }
                message => todo!("unexpected message: {:?}", message),
            }
        }

        panic!("all successors are dead: {}", self.ring)
    }

    pub async fn find_predecessor(&mut self, id: Id) -> Result<Peer> {
        assert!(!self.is_predecessor_of(id));

        let message = Message::FindPredecessor { id };

        if let Some((peer, message)) = self.ring.try_query_successor(&mut self.codec, &message).await {
            trace!("find predecessor with {}", peer);
            match message {
                Message::PredecessorIs { peer } => {
                    return Ok(peer);
                }
                Message::PredecessorIsMe => {
                    return Ok(peer);
                }
                message => todo!("unexpected message: {:?}", message),
            }
        }

        panic!("all successors are dead: {}", self.ring)
    }

    pub async fn find_owner(&mut self, id: Id) -> Result<(Id, NetAddr)> {
        assert!(!self.is_owner_of(id));
        todo!()
    }

    // Must be called periodically
    pub async fn stabilize(&mut self) -> Result<()> {
        let message = Message::Stabilize;

        if let Some((peer, message)) = self.ring.try_query_successor(&mut self.codec, &message).await {
            trace!("stabilize with {}", peer);
            match message {
                Message::StabilizeOk { id, predecessor, successors } => {
                    // If the message contains unexpected data, return early
                    // this node migth fail later, but it's not its fault ;)
                    check_eq!(id, peer.id, Error::InvalidInput)?;
                    check!(successors.len() >= 2, Error::InvalidInput)?;

                    // Once the node has contacted a live successor, it adopts its
                    // successor list (all but last) as its own second and later successors.
                    let successors = successors.iter().copied().take(successors.len() - 1);
                    self.ring.update_successors(&peer, successors);

                    trace!("updated successors: {:?}", self.ring.successors());

                    // Test if the successor’s predecessor might be a better first successor.
                    if self.is_predecessor_of(predecessor.id) {
                        // If so, query its status (liveness) and adopt it as first successor.
                        match net::try_query(&mut self.codec, &predecessor.addr, &Message::Status).await {
                            Ok(Message::Status) => self.ring.push_successor(predecessor),
                            Ok(message) => todo!("unexpected message: {:?}", message),
                            Err(Error::TimedOut) => { /* nothing to do */ }
                            Err(e) => warn!("{}", e),
                        }
                    }

                    // Rectify successor (no reply)
                    let peer = Peer::new(self.id(), self.caddr);
                    let socket = self.ring.successor().addr.connect().await?;
                    let mut channel = Channel::new(socket, &mut self.codec);
                    let message = Message::Rectify { peer };
                    channel.send(&message).await?;

                    return Ok(());
                }
                message => todo!("unexpected message: {:?}", message),
            }
        }

        panic!("all successors are dead: {}", self.ring)
    }

    pub async fn leave(&mut self) -> Result<()> {
        let message = Message::Leave { id: self.id() };

        if let Some((peer, message)) = self.ring.try_query_successor(&mut self.codec, &message).await {
            trace!("leave with {}", peer);
            match message {
                Message::LeaveOk => {
                    debug!("node has left");
                    return Ok(());
                }
                message => todo!("unexpected message: {:?}", message),
            }
        }

        warn!("all successors are dead: {}", self.ring);
        Ok(())
    }
}

impl fmt::Debug for Node {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter
            .debug_struct("Node")
            .field("caddr", &self.caddr)
            .field("paddr", &self.paddr)
            .field("ring", &self.ring)
            .finish_non_exhaustive()
    }
}

impl fmt::Display for Node {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "{}@{}", self.id(), self.caddr)
    }
}

pub struct LockedNode {
    node: RwLock<Node>,
    watch: Watch,
    alive: Cell<bool>,
    guard: Cell<Option<OwnedMutexGuard<()>>>,
}

impl LockedNode {
    pub(crate) fn new(node: Node) -> Self {
        Self { node: RwLock::new(node), watch: Watch::new(), alive: Cell::new(true), guard: Cell::new(None) }
    }

    // either execute a future is I'm the owner of `id`
    // or query the owner with a given message.
    // all atomically, without releasing the lock guard.
    pub async fn with_owner_of<F, T, U>(&self, id: Id, future: T, make: F)
    where
        F: FnOnce(NetAddr) -> U,
        T: Future,
        U: Future,
    {
        let node = self.unlock_shared().await;
        if node.is_owner_of(id) {
            // here for ex. we might insert a value into a local map associated with `id`.
            future.await;
        } else {
            // whereas here we send the value to the owner,
            // and wait for a confirmation that it was handled.
            let mut node = self.unlock_exclusive().await;
            let (_, addr) = node.find_owner(id).await.unwrap();
            // when querying owner here, it might not be the owner anymore,
            // so the protocol must always be implemented in a way to recover from that.
            //
            // query owner to do something, and wait for its reply
            make(addr).await;
        }
        drop(node);
    }

    pub(crate) async fn get_shared<C>(&'_ self, channel: &mut Channel<C>) -> RwLockReadGuard<'_, Node>
    where
        C: Encoder<Item = Message>,
    {
        trace!("waiting for shared access to node");
        if let Some(node) = self.node.try_read() {
            return node;
        }

        loop {
            if let Err(e) = channel.send(&Message::Pending).await {
                warn!("could not send pending message: {}", e);
            }

            match timeout(WAIT_DELAY, self.node.read()).await {
                Ok(node) => break node,
                Err(e) => trace!("{}", e),
            }
        }
    }

    pub(crate) async fn get_exclusive<C>(
        &'_ self, channel: &mut Channel<C>, guard: RwLockReadGuard<'_, Node>,
    ) -> RwLockWriteGuard<'_, Node>
    where
        C: Encoder<Item = Message>,
    {
        // Drop read lock before waiting for write lock
        drop(guard);

        trace!("waiting for exclusive access to node");
        if let Some(node) = self.node.try_write() {
            return node;
        }

        loop {
            if let Err(e) = channel.send(&Message::Pending).await {
                warn!("could not send pending message: {}", e);
            }

            match timeout(WAIT_DELAY, self.node.write()).await {
                Ok(node) => break node,
                Err(e) => trace!("{}", e),
            }
        }
    }

    pub(crate) async fn unlock_shared(&'_ self) -> RwLockReadGuard<'_, Node> {
        self.node.read().await
    }

    pub(crate) async fn unlock_exclusive(&'_ self) -> RwLockWriteGuard<'_, Node> {
        self.node.write().await
    }

    pub fn is_alive(&self) -> bool {
        self.alive.get()
    }

    #[inline]
    pub async fn while_alive<F>(&self, future: F) -> Result<F::Output>
    where
        F: Future,
    {
        self.watch.with(future).await
    }

    pub fn kill(&self) {
        self.alive.set(false);
        self.watch.notify();
    }

    pub async fn leave(self: Rc<Self>) -> Result<()> {
        self.kill();
        {
            let mut node = self.unlock_exclusive().await;
            node.leave().await?;
        }
        self.until_dead().await
    }

    pub async fn until_dead(self: Rc<Self>) -> Result<()> {
        let mutex = Rc::new(Mutex::new(()));
        let guard = Rc::clone(&mutex).try_lock_owned().unwrap();

        // Lock will be dropped when all `Rc<Self>` are dropped.
        self.guard.set(Some(guard));

        let _weak = Rc::downgrade(&self);
        drop(self);

        debug!("waiting for death");
        let _ = mutex.lock().await;
        warn!("node is dead");
        Ok(())
    }
}

impl fmt::Debug for LockedNode {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("LockedNode").field("alive", &self.alive).finish_non_exhaustive()
    }
}

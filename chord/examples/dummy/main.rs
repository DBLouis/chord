#![no_std]
#![no_main]

use chord::NetAddr;
use nuuksio::{net::Ipv4SocketAddr, prelude::*, runtime::Runtime, time::sleep};
use ros::{error::Result, io::StdErr, prelude::*, time::Duration};

// Ring-maintenance port
const CHORD_PORT: u16 = 8000;
// Application port
const DUMMY_PORT: u16 = 9000;

ros::main!(|args: &[&str]| {
    let mut args = args.into_iter().skip(1).copied();

    match args.next().unwrap() {
        "--boot" | "-b" => {
            let index: usize = args.next().unwrap().parse().unwrap();
            let mut addrs: Vec<_> =
                args.map(|arg| arg.parse().map(|ip| Ipv4SocketAddr::new(ip, CHORD_PORT))).collect::<Result<_>>()?;
            // Rotate so that `index` becomes the last element.
            addrs.rotate_left(index + 1);
            let me = addrs.pop().unwrap();
            boot(me, addrs)
        }
        "--join" | "-j" => {
            let me = args.next().unwrap().parse().map(|ip| Ipv4SocketAddr::new(ip, CHORD_PORT)).unwrap();
            let entry = args.next().unwrap().parse().map(|ip| Ipv4SocketAddr::new(ip, CHORD_PORT)).unwrap();
            let delay = args.next().map(|d| d.parse().unwrap()).unwrap_or(0);
            let delay = Duration::from_secs(delay);
            join(me, entry, delay)
        }
        _ => unimplemented!(),
    }
});

fn boot(me: Ipv4SocketAddr, others: Vec<Ipv4SocketAddr>) -> Result<()> {
    let rt = Runtime::new(StdErr)?;
    let me2 = Ipv4SocketAddr::new(*me.ip(), DUMMY_PORT);
    let others = others.into_iter().map(NetAddr::from).collect();

    rt.block_on(async move {
        info!(rt; "boot {} with {:?}", me, others);
        let node = chord::bootstrap(me.into(), me2.into(), others).await?;
        node.until_dead().await?;
        info!(rt; "boot exit");
        rt.shutdown();
        Ok(())
    })
    .unwrap()
}

fn join(me: Ipv4SocketAddr, entry: Ipv4SocketAddr, delay: Duration) -> Result<()> {
    let rt = Runtime::new(StdErr)?;
    let me2 = Ipv4SocketAddr::new(*me.ip(), DUMMY_PORT);

    rt.block_on(async move {
        sleep(delay).await?;
        info!(rt; "join {}", me);
        let node = chord::join(me.into(), me2.into(), entry.into()).await?;
        node.until_dead().await?;
        info!(rt; "join exit");
        rt.shutdown();
        Ok(())
    })
    .unwrap()
}

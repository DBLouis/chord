#!/bin/bash -eu

DOCKER=podman
EXAMPLE=dummy
IMAGE="localhost/rusty-chord/$EXAMPLE"

command -v podman >/dev/null
command -v multitail >/dev/null

case "${1-}" in
    'build' | 'x')
        cargo build --package rusty-chord --release --target x86_64-unknown-linux-musl --example $EXAMPLE
        $DOCKER image rm $IMAGE
        $DOCKER build --tag $IMAGE --build-arg example=$EXAMPLE .
        ;;

    'setup')
        $DOCKER network create --internal --subnet 172.20.0.0/16 chord
        ;;

    'boot' | 'b')
        rm -f /tmp/rc-boot-*.log
        peers=("172.20.0.11" "172.20.0.12" "172.20.0.13" "172.20.0.14")
        for ((i = 0; i < ${#peers[@]}; i++ ))
        do
            echo "Starting container for boot node '${peers[i]}'"
            $DOCKER run --rm --tty --network chord --ip="${peers[i]}" $IMAGE --boot "$i" "${peers[@]}" &>"/tmp/rc-boot-$i.log" &
        done
        ;;

    'join' | 'j')
        peer="172.20.0.$2"
        entry="172.20.0.$3"
        echo "Starting container for join node '${peer}' with entry '${entry}'"
        $DOCKER run --rm --tty --network chord --ip="${peer}" $IMAGE --join "${peer}" "${entry}" 10 &>"/tmp/rc-join-$2.log" &
        ;;

    'list' | 'l')
        $DOCKER ps -a
        ;;

    'kill' | 'k')
        $DOCKER kill -a >/dev/null
        ;;

    'show' | 's')
        if [ -n "${2-}" ]
        then
            multitail -F .multitailrc -CS rusty "/tmp/rc-join-$2.log"
        else
            multitail -F .multitailrc -s 2 -CS rusty /tmp/rc-boot-*.log
        fi
        ;;
    *)
        echo "Usage: ${BASH_SOURCE[0]} [build|setup|boot|join|list|kill|show]"
        ;;
esac

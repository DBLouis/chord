use crate::{
    executor::Executor,
    log::Level,
    prelude::*,
    reactor::{Interest, Reactor},
    task::{make_waker, Handle, RawHandle, Task, TaskId},
};
use ros::{
    cell::{unsync::OnceCell, Cell, RefCell},
    error::{Error, Result},
    fmt,
    future::Future,
    io::Fd,
    panic,
    prelude::*,
    task::{Context, Poll, Waker},
};

mod_level!(Level::Debug);

#[thread_local]
static STATE: OnceCell<State> = OnceCell::new();

/// A handle to the runtime.
// This type serves as a proof that STATE has been initialized
// The non-mutable OnceCell ensure that STATE is initialized once
// and not dropped until thread exits
#[derive(Debug, Clone, Copy)]
#[repr(transparent)]
pub struct Runtime {
    // This type must only be constructed if runtime state is initialized
    _private: (),
}

impl Runtime {
    pub fn new<W>(writer: W) -> Result<Self>
    where
        W: fmt::Write + 'static,
    {
        let state = State::new(Box::new(writer))?;
        STATE.set(state).expect("runtime already initialized");

        panic::set_thread_hook(Box::new(move |info| {
            let rt = Runtime::get();
            error!(rt; "{}", info);
        }));

        Ok(Self { _private: () })
    }

    pub fn get() -> Self {
        STATE.get().map(|_| Self { _private: () }).expect("runtime not initialized")
    }

    /// # Safety
    ///
    /// Caller must ensure that runtime is initialized
    pub unsafe fn get_unchecked() -> Self {
        debug_assert!(STATE.get().is_some());
        Self { _private: () }
    }

    #[inline(always)]
    fn with<F, R>(&self, f: F) -> R
    where
        F: FnOnce(&State) -> R,
    {
        // SAFETY: STATE is initialized if this type is contructed
        let state = unsafe { STATE.get().unwrap_unchecked() };
        f(state)
    }

    pub fn log(&self, args: fmt::Arguments<'_>) {
        self.with(|state| state.writer.borrow_mut().write_fmt(args).unwrap());
    }

    pub fn block_on<F>(&self, future: F) -> Option<F::Output>
    where
        F: Future + 'static,
        F::Output: 'static,
    {
        let output = Cell::new(None);
        let handle = self.spawn({
            let output = output.as_ptr();
            async move { unsafe { output.write(Some(future.await)) } }
        });
        // FIXME what is preventing run from being called multiple times?
        self.run();
        drop(handle);
        output.take()
    }

    pub fn spawn<F>(&self, future: F) -> Handle<F::Output>
    where
        F: Future + 'static,
        F::Output: 'static,
    {
        let task = Task::new(*self, future);
        let (handle, raw_handle) = task.into_handles();
        let tid = handle.tid();

        mod_trace!(self; "spawn task {}", tid);

        self.with(|state| state.executor.borrow_mut().add(raw_handle));
        handle
    }

    pub fn register(&self, fd: Fd, interest: Interest, waker: Waker) {
        mod_trace!(self; "register {} {}", fd, interest);
        self.with(|state| state.reactor.borrow_mut().register(fd, interest, waker))
    }

    pub fn unregister(&self, fd: Fd) {
        mod_trace!(self; "unregister {}", fd);
        self.with(|state| state.reactor.borrow_mut().unregister(fd))
    }

    pub fn wake(&self, tid: TaskId) {
        mod_trace!(self; "wake task {}", tid);
        self.with(|state| state.executor.borrow_mut().wake(tid))
    }

    pub fn shutdown(&self) {
        self.with(|state| state.running.set(false))
    }

    pub fn is_running(&self) -> bool {
        self.with(|state| state.running.get())
    }

    fn awake(&self) -> Option<RawHandle> {
        self.with(|state| state.executor.borrow_mut().pop())
    }

    fn push_idle(&self, handle: RawHandle) {
        self.with(|state| state.executor.borrow_mut().push(handle))
    }

    fn wake_ready(&self) -> Result<()> {
        self.with(|state| state.reactor.borrow_mut().wake_ready())
    }

    pub fn get_tid(&self) -> TaskId {
        self.with(|state| state.tid.get())
    }

    fn set_tid(&self, tid: TaskId) {
        self.with(|state| state.tid.set(tid));
    }

    fn unset_tid(&self) {
        self.with(|state| state.tid.set(TaskId::ZERO));
    }

    fn run(&self) {
        info!("runtime starting");

        while self.is_running() {
            while let Some(handle) = self.awake() {
                let tid = handle.tid();
                self.set_tid(tid);

                mod_trace!(self; "task is awake");

                let waker = make_waker(*self, tid);
                let mut cx = Context::from_waker(&waker);

                match handle.poll(&mut cx) {
                    Poll::Pending => {
                        mod_trace!(self; "task is pending");
                        self.push_idle(handle);
                    }
                    Poll::Ready(_) => {
                        mod_trace!(self; "task is ready");
                    }
                }

                self.unset_tid();
            }
            match self.wake_ready() {
                Err(Error::TimedOut) => mod_trace!(*self; "timed out"),
                Err(e) => panic!("{}", e),
                _ => {}
            }
        }

        info!("runtime stopping");
    }
}

pub struct State {
    // Identifier of task currently being polled.
    tid: Cell<TaskId>,
    running: Cell<bool>,
    executor: RefCell<Executor>,
    reactor: RefCell<Reactor>,
    writer: RefCell<Box<dyn fmt::Write>>,
}

impl State {
    pub fn new(writer: Box<dyn fmt::Write>) -> Result<Self> {
        let tid = Cell::new(TaskId::ZERO);
        let running = Cell::new(true);
        let reactor = RefCell::new(Reactor::new()?);
        let executor = RefCell::new(Executor::new());
        let writer = RefCell::new(writer);
        Ok(Self { tid, running, executor, reactor, writer })
    }
}

impl fmt::Debug for State {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter
            .debug_struct("State")
            .field("running", &self.running)
            .field("executor", &self.executor)
            .field("reactor", &self.reactor)
            .finish_non_exhaustive()
    }
}

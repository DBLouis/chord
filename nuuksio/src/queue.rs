//! Single-threaded FIFO queue using a "magic ring buffer".

mod file;
mod raw;
mod region;

use raw::RawQueue;
use ros::{alloc::Layout, error::Result, fmt, marker::PhantomData, prelude::*};

pub use raw::RawQueue as ByteQueue;

pub struct Queue<T> {
    inner: RawQueue,
    _phantom: PhantomData<T>,
}

impl<T> Queue<T> {
    pub fn new() -> Result<Self> {
        Ok(Self { inner: RawQueue::new()?, _phantom: PhantomData })
    }

    pub fn with_capacity(capacity: usize) -> Result<Self> {
        Ok(Self { inner: RawQueue::with_capacity(capacity)?, _phantom: PhantomData })
    }

    pub fn push(&mut self, value: T) -> Option<T> {
        let layout = Layout::new::<T>();
        if let Some(space) = self.inner.try_push(layout) {
            let ptr = space.cast::<T>().as_ptr();
            unsafe { ptr.write(value) };
            None
        } else {
            Some(value)
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        let layout = Layout::new::<T>();
        let space = self.inner.try_pop(layout)?;
        let ptr = space.cast::<T>().as_ptr();
        Some(unsafe { ptr.read() })
    }

    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }

    pub fn is_full(&self) -> bool {
        self.inner.is_full()
    }
}

impl<T> fmt::Debug for Queue<T> {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("Queue").field("inner", &self.inner).finish_non_exhaustive()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test_case]
    fn empty() {
        let q: Queue<i32> = Queue::new().unwrap();
        assert!(q.is_empty());
    }

    #[test_case]
    fn push() {
        let mut q: Queue<i32> = Queue::new().unwrap();
        q.push(42);
        assert!(!q.is_empty());
    }

    #[test_case]
    fn pop() {
        let mut q: Queue<i32> = Queue::new().unwrap();
        q.push(4);
        q.push(2);
        assert_eq!(Some(4), q.pop());
        assert_eq!(Some(2), q.pop());
        assert_eq!(None, q.pop());
        assert!(q.is_empty());
    }
}

use super::semaphore::Semaphore;
use ros::{
    cell::UnsafeCell,
    ops::{Deref, DerefMut},
    rc::Rc,
};

#[derive(Debug)]
pub struct Mutex<T: ?Sized> {
    semaphore: Semaphore,
    value: UnsafeCell<T>,
}

impl<T> Mutex<T> {
    pub fn new(value: T) -> Self {
        Self { semaphore: Semaphore::new(1), value: UnsafeCell::new(value) }
    }
}

impl<T: ?Sized> Mutex<T> {
    pub async fn lock(&self) -> MutexGuard<'_, T> {
        self.semaphore.acquire(1).await;
        MutexGuard { mutex: self }
    }

    pub fn try_lock(&self) -> Option<MutexGuard<'_, T>> {
        if self.semaphore.try_acquire(1) {
            Some(MutexGuard { mutex: self })
        } else {
            None
        }
    }

    pub async fn lock_owned(self: Rc<Self>) -> OwnedMutexGuard<T> {
        self.semaphore.acquire(1).await;
        OwnedMutexGuard { mutex: self }
    }

    pub fn try_lock_owned(self: Rc<Self>) -> Option<OwnedMutexGuard<T>> {
        if self.semaphore.try_acquire(1) {
            Some(OwnedMutexGuard { mutex: self })
        } else {
            None
        }
    }
}

#[must_use]
#[derive(Debug)]
pub struct MutexGuard<'a, T: ?Sized> {
    pub(super) mutex: &'a Mutex<T>,
}

impl<'a, T: ?Sized> Deref for MutexGuard<'a, T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.mutex.value.get() }
    }
}

impl<'a, T: ?Sized> DerefMut for MutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.mutex.value.get() }
    }
}

impl<'a, T: ?Sized> Drop for MutexGuard<'a, T> {
    fn drop(&mut self) {
        self.mutex.semaphore.release(1);
    }
}

#[must_use]
#[derive(Debug)]
pub struct OwnedMutexGuard<T: ?Sized> {
    mutex: Rc<Mutex<T>>,
}

impl<T: ?Sized> Deref for OwnedMutexGuard<T> {
    type Target = T;

    fn deref(&self) -> &T {
        unsafe { &*self.mutex.value.get() }
    }
}

impl<T: ?Sized> DerefMut for OwnedMutexGuard<T> {
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.mutex.value.get() }
    }
}

impl<T: ?Sized> Drop for OwnedMutexGuard<T> {
    fn drop(&mut self) {
        self.mutex.semaphore.release(1);
    }
}

use intrusive_collections::{intrusive_adapter, LinkedList, LinkedListLink, UnsafeRef};
use ros::{
    cell::{Cell, UnsafeCell},
    future::Future,
    pin::Pin,
    task::{Context, Poll, Waker},
};

intrusive_adapter!(WaiterAdapter = UnsafeRef<Waiter>: Waiter { link: LinkedListLink });

struct Waiter {
    link: LinkedListLink,
    waker: Waker,
}

impl From<Waker> for Waiter {
    fn from(waker: Waker) -> Self {
        Self { link: LinkedListLink::new(), waker }
    }
}

#[derive(Debug)]
pub struct Semaphore {
    queue: UnsafeCell<LinkedList<WaiterAdapter>>,
    permits: Cell<usize>,
}

impl Semaphore {
    pub fn new(permits: usize) -> Self {
        Self { queue: UnsafeCell::new(LinkedList::new(WaiterAdapter::new())), permits: Cell::new(permits) }
    }

    pub async fn acquire(&self, amount: usize) {
        Acquire::new(self, amount).await
    }

    pub fn try_acquire(&self, amount: usize) -> bool {
        self.try_take(amount)
    }

    fn try_take(&self, amount: usize) -> bool {
        let permits = self.permits.get();
        let (remaining, overflow) = permits.overflowing_sub(amount);
        if !overflow {
            self.permits.set(remaining);
        }
        !overflow
    }

    pub fn release(&self, amount: usize) {
        let permits = self.permits.get();
        self.permits.set(permits.checked_add(amount).unwrap());

        // Safety: TODO
        let queue = unsafe { &mut *self.queue.get() };

        if let Some(waiter) = queue.pop_front() {
            waiter.waker.wake_by_ref();
        }
    }
}

struct Acquire<'a> {
    semaphore: &'a Semaphore,
    waiter: Option<Waiter>,
    amount: usize,
}

impl<'a> Acquire<'a> {
    fn new(semaphore: &'a Semaphore, amount: usize) -> Self {
        Self { semaphore, waiter: None, amount }
    }
}

impl<'a> Future for Acquire<'a> {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.semaphore.try_take(self.amount) {
            // TODO can there be pointer to `self.waiter` in `queue`?
            return Poll::Ready(());
        }

        let waker = cx.waker().clone();
        let _ = self.waiter.replace(Waiter::from(waker));

        let waiter = unsafe {
            let waiter = self.waiter.as_ref().unwrap();
            // Safety: TODO
            UnsafeRef::from_raw(waiter as *const Waiter)
        };

        // Safety: TODO
        let queue = unsafe { &mut *self.semaphore.queue.get() };
        queue.push_back(waiter);

        Poll::Pending
    }
}

//! A single-threaded runtime for asynchronous applications.
#![cfg(unix)]
#![no_std]
#![cfg_attr(not(test), no_main)]
// TODO backup and replace magic buffer with simple buffer (to remove features)
#![feature(layout_for_ptr)] // DynQueue
#![feature(nonnull_slice_from_raw_parts)] // queue::Region
#![feature(ptr_metadata)] // DynQueue
#![feature(slice_ptr_len)] // queue::Region
#![feature(thread_local)]
#![feature(unsize)] // DynQueue
#![warn(missing_debug_implementations)]

pub mod executor;
pub mod future;
pub mod log;
pub mod net;
pub mod prelude;
pub mod queue;
pub mod reactor;
pub mod runtime;
pub mod signal;
pub mod sync;
pub mod task;
pub mod time;

#[cfg(test)]
mod test;

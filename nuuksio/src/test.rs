use crate::runtime::Runtime;
use ros::prelude::*;

#[test_case]
fn trivial() {
    let _rt = Runtime::get();
    assert_eq!(1, 1);
}

#[cfg(test)]
ros::main!(|_args: &[&str]| {
    let _rt = Runtime::new(ros::io::Void);
    crate::test_harness();
    Ok(())
});

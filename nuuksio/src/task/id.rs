use ros::{fmt, hash::Hash};

/// An opaque task identifier.
// This refers to one task, but a pointer to it cannot be obtain because,
// contrarily to Handle and RawHandle, nothing ensure that the task still exists.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct TaskId {
    value: usize,
}

impl TaskId {
    pub const ZERO: Self = Self { value: 0 };
}

impl From<usize> for TaskId {
    fn from(value: usize) -> Self {
        debug_assert_ne!(value, 0);
        Self { value }
    }
}

impl From<TaskId> for usize {
    fn from(tid: TaskId) -> usize {
        tid.value
    }
}

impl fmt::Display for TaskId {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.value == 0 {
            write!(formatter, "NONE")
        } else {
            let id = (self.value & 0xFFFF) >> 4;
            write!(formatter, "#{:03X}", id)
        }
    }
}

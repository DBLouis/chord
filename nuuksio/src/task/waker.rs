use super::TaskId;
use crate::runtime::Runtime;
use ros::task::{RawWaker, RawWakerVTable, Waker};

fn make_raw_waker(_rt: Runtime, tid: TaskId) -> RawWaker {
    const VTABLE: RawWakerVTable = RawWakerVTable::new(clone, wake, wake, no_op);

    fn clone(arg: *const ()) -> RawWaker {
        let tid = TaskId::from(arg as usize);
        // SAFETY: `raw_waker` takes a Runtime
        let rt = unsafe { Runtime::get_unchecked() };
        make_raw_waker(rt, tid)
    }

    fn wake(arg: *const ()) {
        let tid = TaskId::from(arg as usize);
        // SAFETY: `raw_waker` takes a Runtime
        let rt = unsafe { Runtime::get_unchecked() };
        rt.wake(tid);
    }

    fn no_op(_: *const ()) {}

    let arg = usize::from(tid) as *const ();
    RawWaker::new(arg, &VTABLE)
}

pub fn make_waker(rt: Runtime, tid: TaskId) -> Waker {
    unsafe { Waker::from_raw(make_raw_waker(rt, tid)) }
}

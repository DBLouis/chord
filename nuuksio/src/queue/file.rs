use super::region::Region;
use ros::{
    error::{Error, Result},
    ffi::CStr,
    io::Fd,
};

/// Memory-mapped anonymous file for "magic ring buffer".
#[derive(Debug)]
pub struct File {
    fd: Fd,
    pub(super) base: Region,
}

impl File {
    pub fn new() -> Result<Self> {
        // Round up to one page size
        Self::with_capacity(1)
    }

    pub fn with_capacity(mut capacity: usize) -> Result<Self> {
        assert!(capacity.is_power_of_two());

        let page_size = page_size()?;

        while capacity < page_size {
            capacity *= 2;
        }

        assert!(capacity <= i64::MAX as usize);

        let fd = file_create()?;
        file_truncate(fd, capacity)?;

        let base = Region::new(fd, capacity)?;

        Ok(Self { fd, base })
    }

    pub fn capacity(&self) -> usize {
        self.base.capacity()
    }
}

impl Drop for File {
    fn drop(&mut self) {
        let _ = unsafe { libc::close(self.fd) };
    }
}

fn page_size() -> Result<usize> {
    let value = unsafe { libc::sysconf(libc::_SC_PAGE_SIZE) };
    if value == -1 {
        Err(Error::last())
    } else {
        Ok(value as usize)
    }
}

fn file_create() -> Result<Fd> {
    let name = CStr::from_bytes_with_nul(b"magic\0").unwrap();
    let fd = unsafe { libc::syscall(libc::SYS_memfd_create, name.as_ptr(), libc::MFD_CLOEXEC) };
    if fd == -1 {
        Err(Error::last())
    } else {
        Ok(fd as Fd)
    }
}

fn file_truncate(fd: Fd, capacity: usize) -> Result<()> {
    let rv = unsafe { libc::ftruncate(fd, capacity as i64) };
    if rv == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test_case]
    fn new() {
        let _f = File::new().unwrap();
    }

    #[test_case]
    fn with_capacity() {
        let _f = File::with_capacity(1 << 16).unwrap();
    }
}

use libc::{MAP_ANONYMOUS, MAP_FAILED, MAP_FIXED, MAP_PRIVATE, MAP_SHARED, PROT_NONE, PROT_READ, PROT_WRITE};
use ros::{
    error::{Error, Result},
    fmt,
    io::Fd,
    ptr,
    ptr::NonNull,
};

pub struct Region {
    // Buffer of length equal to 2 * capacity
    data: NonNull<[u8]>,
}

impl Region {
    pub fn new(fd: Fd, capacity: usize) -> Result<Self> {
        assert!(capacity.is_power_of_two());

        let ptr = mmap(ptr::null_mut(), 2 * capacity, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)?;

        {
            let _ = mmap(ptr, capacity, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd, 0)?;
            let ptr = unsafe { ptr.add(capacity) };
            let _ = mmap(ptr, capacity, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, fd, 0)?;
        }

        let ptr = NonNull::new(ptr).unwrap();

        Ok(Self { data: NonNull::slice_from_raw_parts(ptr.cast(), 2 * capacity) })
    }

    pub fn capacity(&self) -> usize {
        self.data.len() >> 1
    }

    // get a pointer at `offset`; valid for `capacity` bytes
    pub fn get(&self, offset: usize) -> *mut u8 {
        let capacity = self.capacity();
        debug_assert!(offset < capacity);
        let offset = offset.min(capacity);
        unsafe { self.data.cast::<u8>().as_ptr().add(offset) }
    }
}

unsafe impl Send for Region {}

impl fmt::Debug for Region {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("Region").field("data", &self.data).field("capacity", &self.capacity()).finish()
    }
}

impl Drop for Region {
    fn drop(&mut self) {
        let ptr = self.data.cast().as_ptr();
        let size = self.data.len();
        let _ = unsafe { libc::munmap(ptr, size) };
    }
}

fn mmap(
    ptr: *mut libc::c_void, size: usize, prot: libc::c_int, flags: libc::c_int, fd: Fd, offset: libc::off_t,
) -> Result<*mut libc::c_void> {
    let ptr = unsafe { libc::mmap(ptr, size, prot, flags, fd, offset) };
    if ptr == MAP_FAILED {
        Err(Error::last())
    } else {
        Ok(ptr)
    }
}

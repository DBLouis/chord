use crate::{
    prelude::*,
    task::{RawHandle, TaskId},
};
use ros::{
    collections::{HashMap, VecDeque},
    fmt,
    prelude::*,
};

#[derive(Debug, Default)]
pub struct Executor {
    // Idle tasks
    tasks: HashMap<TaskId, RawHandle>,
    // Awake tasks
    awake: VecDeque<TaskId>,
}

impl Executor {
    pub fn new() -> Self {
        Self { tasks: HashMap::new(), awake: VecDeque::new() }
    }

    pub fn wake(&mut self, tid: TaskId) {
        self.awake.push_back(tid);
    }

    pub fn add(&mut self, handle: RawHandle) {
        let tid = handle.tid();
        if self.tasks.insert(tid, handle).is_some() {
            panic!("duplicate task {}", tid);
        }
        // Schedule task to be polled once
        self.wake(tid);
    }

    pub fn pop(&mut self) -> Option<RawHandle> {
        let tid = self.awake.pop_front()?;

        if let Some(handle) = self.tasks.remove(&tid) {
            debug_assert_eq!(tid, handle.tid());
            Some(handle)
        } else {
            warn!("unknown task {}", tid);
            None
        }
    }

    pub fn push(&mut self, handle: RawHandle) {
        self.tasks.insert(handle.tid(), handle);
    }
}

impl fmt::Display for Executor {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(formatter, "Executor")
    }
}

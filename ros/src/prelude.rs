pub use crate::{
    check, check_eq, check_ne, eprint, eprintln,
    io::{StdErr, StdOut},
    print, println,
    process::UnwrapExt,
};

pub use __core::prelude::v1::*;

pub use __alloc::{borrow::ToOwned, boxed::Box, string::String, string::ToString, vec::Vec};

// TODO only import vec macro, not vec module?
pub use __alloc::{format, vec};

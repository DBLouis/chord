pub use __core::panic::*;

use crate::{cell::RefCell, prelude::*};

pub type Hook = Box<dyn Fn(&PanicInfo<'_>) + 'static>;

#[thread_local]
static HOOK: RefCell<Option<Hook>> = RefCell::new(None);

pub fn set_thread_hook(hook: Hook) {
    let _ = HOOK.borrow_mut().replace(hook);
}

pub fn take_thread_hook() -> Option<Hook> {
    HOOK.borrow_mut().take()
}

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo<'_>) -> ! {
    if let Some(hook) = &*HOOK.borrow() {
        hook(info)
    }
    crate::process::abort();
}

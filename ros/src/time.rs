use crate::{fmt, mem, ops};

#[derive(Debug, Clone, Copy)]
pub struct Duration {
    inner: Time,
}

impl Duration {
    pub const ZERO: Self = Self { inner: Time::ZERO };

    pub const fn from_nanos(nanos: u64) -> Self {
        Self { inner: Time::from_nanos(nanos) }
    }

    pub const fn from_micros(micros: u64) -> Self {
        Self { inner: Time::from_micros(micros) }
    }

    pub const fn from_millis(millis: u64) -> Self {
        Self { inner: Time::from_millis(millis) }
    }

    pub const fn from_secs(secs: u64) -> Self {
        Self { inner: Time::from_secs(secs) }
    }

    #[inline]
    pub fn as_real_secs(&self) -> f64 {
        self.inner.as_real_secs()
    }

    #[inline]
    pub const fn as_secs(&self) -> u64 {
        self.inner.as_secs()
    }

    #[inline]
    pub const fn as_nanos(&self) -> u128 {
        self.inner.as_nanos()
    }

    #[inline]
    pub const fn as_micros(&self) -> u128 {
        self.inner.as_micros()
    }

    #[inline]
    pub const fn as_millis(&self) -> u128 {
        self.inner.as_millis()
    }

    #[inline]
    pub const fn subsec_nanos(&self) -> u32 {
        self.inner.subsec_nanos()
    }

    #[inline]
    pub const fn subsec_micros(&self) -> u32 {
        self.inner.subsec_micros()
    }

    #[inline]
    pub const fn subsec_millis(&self) -> u32 {
        self.inner.subsec_millis()
    }

    #[doc(hidden)]
    pub fn as_raw(&self) -> *const libc::timespec {
        &*self.inner
    }
}

impl fmt::Display for Duration {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let precision = formatter.precision().unwrap_or(2);
        let s = self.as_secs();
        let n = self.subsec_nanos();
        if s > 0 {
            let d = 10_u32.pow(9 - precision as u32);
            write!(formatter, "{}.{}s", s, n / d)
        } else if n < 1_000 {
            write!(formatter, "{}ns", n)
        } else if n < 1_000_000 {
            write!(formatter, "{}µs", n / 1_000)
        } else {
            write!(formatter, "{}ms", n / 1_000_000)
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Instant {
    inner: Time,
}

impl Instant {
    pub fn now() -> Self {
        let inner = Time::now(libc::CLOCK_MONOTONIC);
        Self { inner }
    }

    pub fn elapsed(&self) -> Duration {
        let inner = Time::now(libc::CLOCK_MONOTONIC) - self.inner;
        Duration { inner }
    }
}

impl ops::Add<Duration> for Instant {
    type Output = Instant;

    fn add(self, rhs: Duration) -> Instant {
        Instant { inner: self.inner + rhs.inner }
    }
}

impl ops::Sub for Instant {
    type Output = Duration;

    fn sub(self, rhs: Instant) -> Duration {
        Duration { inner: self.inner - rhs.inner }
    }
}

#[derive(Debug)]
pub struct Timestamp {
    inner: Time,
}

impl Timestamp {
    pub fn now() -> Self {
        let inner = Time::now(libc::CLOCK_REALTIME);
        Self { inner }
    }

    #[doc(hidden)]
    pub fn as_raw(&self) -> *const libc::timespec {
        &*self.inner
    }
}

impl ops::Add<Duration> for Timestamp {
    type Output = Self;

    fn add(self, rhs: Duration) -> Self {
        Self { inner: self.inner + rhs.inner }
    }
}

impl fmt::Display for Timestamp {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO proper date+time formatting using `strftime`
        write!(formatter, "{:09}.{:06}", self.inner.as_secs(), self.inner.subsec_micros())
    }
}

#[derive(Clone, Copy)]
pub struct Time {
    inner: libc::timespec,
}

impl Time {
    pub const ZERO: Self = Self::from_nanos(0);
    pub const EPSILON: Self = Self::from_nanos(1);

    pub fn now(id: libc::clockid_t) -> Self {
        // Safety: libc::timespec can be zeroed
        let mut inner: libc::timespec = unsafe { mem::zeroed() };

        let rv = unsafe { libc::clock_gettime(id, &mut inner as *mut _) };
        // In release mode a failure here will return an zeroed timespec
        // This is safe but will likely cause logical errors
        debug_assert_ne!(rv, -1);

        debug_assert!(inner.tv_sec >= 0);
        debug_assert!(inner.tv_nsec >= 0);
        debug_assert!(inner.tv_nsec < 1_000_000_000);

        Self { inner }
    }

    pub const fn from_nanos(nanos: u64) -> Self {
        Self {
            inner: libc::timespec { tv_sec: (nanos / 1_000_000_000) as i64, tv_nsec: (nanos % 1_000_000_000) as i64 },
        }
    }

    pub const fn from_micros(micros: u64) -> Self {
        Self {
            inner: libc::timespec { tv_sec: (micros / 1_000_000) as i64, tv_nsec: (micros % 1_000_000) as i64 * 1_000 },
        }
    }

    const fn from_millis(millis: u64) -> Self {
        Self { inner: libc::timespec { tv_sec: (millis / 1_000) as i64, tv_nsec: (millis % 1_000) as i64 * 1_000_000 } }
    }

    const fn from_secs(secs: u64) -> Self {
        Self { inner: libc::timespec { tv_sec: secs as i64, tv_nsec: 0 } }
    }

    fn as_real_secs(&self) -> f64 {
        self.inner.tv_sec as f64 + self.inner.tv_nsec as f64 / 1_000_000_000_f64
    }

    const fn as_secs(&self) -> u64 {
        self.inner.tv_sec as u64
    }

    const fn as_nanos(&self) -> u128 {
        self.inner.tv_sec as u128 * 1_000_000_000 + self.inner.tv_nsec as u128
    }

    const fn as_micros(&self) -> u128 {
        self.inner.tv_sec as u128 * 1_000_000 + self.inner.tv_nsec as u128 / 1_000
    }

    const fn as_millis(&self) -> u128 {
        self.inner.tv_sec as u128 * 1_000 + self.inner.tv_nsec as u128 / 1_000_000
    }

    const fn subsec_nanos(&self) -> u32 {
        self.inner.tv_nsec as u32
    }

    const fn subsec_micros(&self) -> u32 {
        self.inner.tv_nsec as u32 / 1_000
    }

    const fn subsec_millis(&self) -> u32 {
        self.inner.tv_nsec as u32 / 1_000_000
    }
}

impl From<libc::timespec> for Time {
    fn from(ts: libc::timespec) -> Self {
        Self { inner: ts }
    }
}

impl ops::Deref for Time {
    type Target = libc::timespec;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl ops::Add for Time {
    type Output = Time;

    fn add(self, rhs: Time) -> Time {
        let nanos = self.as_nanos().checked_add(rhs.as_nanos()).unwrap();
        debug_assert!(nanos < u64::MAX as u128);
        Time::from_nanos(nanos as u64)
    }
}

impl ops::Sub for Time {
    type Output = Time;

    fn sub(self, rhs: Time) -> Time {
        let nanos = self.as_nanos().checked_sub(rhs.as_nanos()).unwrap();
        debug_assert!(nanos < u64::MAX as u128);
        Time::from_nanos(nanos as u64)
    }
}

impl fmt::Debug for Time {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter
            .debug_struct("Time")
            .field("seconds", &self.as_secs())
            .field("nanoseconds", &self.subsec_nanos())
            .finish()
    }
}

#[derive(Clone, Copy)]
pub struct Expiration {
    inner: libc::itimerspec,
}

impl Expiration {
    pub fn interval(duration: Duration) -> Self {
        Self { inner: libc::itimerspec { it_interval: *duration.inner, it_value: *Time::EPSILON } }
    }

    pub fn interval_at(instant: Instant, duration: Duration) -> Self {
        Self { inner: libc::itimerspec { it_interval: *duration.inner, it_value: *instant.inner } }
    }

    pub fn one_shot(duration: Duration) -> Self {
        Self { inner: libc::itimerspec { it_interval: *Time::ZERO, it_value: *duration.inner } }
    }

    pub fn at(instant: Instant) -> Self {
        Self { inner: libc::itimerspec { it_interval: *Time::ZERO, it_value: *instant.inner } }
    }
}

impl ops::Deref for Expiration {
    type Target = libc::itimerspec;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl fmt::Debug for Expiration {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter
            .debug_struct("Expiration")
            .field("initial", &Time::from(self.it_value))
            .field("interval", &Time::from(self.it_interval))
            .finish()
    }
}

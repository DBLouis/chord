use crate::{
    error::{Error, Result},
    fmt,
    marker::PhantomData,
    mem::MaybeUninit,
    prelude::*,
    ptr,
    ptr::DynMetadata,
    time::Duration,
};

trait Closure {
    fn call(self: Box<Self>) -> *mut ();
}

impl<F> Closure for Wrapper<F>
where
    F: FnOnce() -> *mut (),
{
    fn call(self: Box<Self>) -> *mut () {
        (self.inner)()
    }
}

#[repr(C)]
struct Wrapper<F> {
    // must be first member
    vtable: DynMetadata<dyn Closure>,
    inner: F,
}

impl<F> Wrapper<F>
where
    F: FnOnce() -> *mut () + 'static,
{
    fn new(inner: F) -> Box<Self> {
        let vtable = ptr::metadata({
            // Get pointer to vtable of dyn object
            ptr::null::<Self>() as *const dyn Closure
        });
        Box::new(Self { vtable, inner })
    }
}

impl<F> Wrapper<F> {
    unsafe fn from_raw(ptr: *mut Self) -> Box<dyn Closure> {
        // Read first member of `Wrapper` which is the vtable pointer
        let vtable = (ptr as *const DynMetadata<dyn Closure>).read();
        // Create dyn trait object
        let f = ptr::from_raw_parts_mut(ptr as *mut (), vtable);
        Box::from_raw(f as *mut dyn Closure)
    }
}

pub struct Thread<R> {
    tid: libc::pthread_t,
    _r: PhantomData<R>,
}

impl<R> Thread<R>
where
    R: Send + 'static,
{
    pub fn spawn<F>(f: F) -> Result<Self>
    where
        F: FnOnce() -> R,
        F: Send + 'static,
    {
        let f = move || {
            let r = f();
            Box::into_raw(Box::new(r)) as *mut ()
        };
        let f = Box::into_raw(Wrapper::new(f));

        let mut tid = MaybeUninit::uninit();

        let rv = unsafe {
            let arg = f as *mut libc::c_void;
            libc::pthread_create(tid.as_mut_ptr(), ptr::null(), entry, arg)
        };

        if rv != 0 {
            unsafe { Box::from_raw(f as *mut F) };
            return Err(Error::from(rv));
        }

        let tid = unsafe { tid.assume_init() };
        Ok(Self { tid, _r: PhantomData })
    }

    pub fn join(self) -> Result<Box<R>> {
        let mut r = MaybeUninit::uninit();

        let rv = unsafe { libc::pthread_join(self.tid, r.as_mut_ptr()) };

        if rv != 0 {
            return Err(Error::from(rv));
        }

        let r = unsafe { r.assume_init() };

        if r.is_null() {
            return Err(Error::InvalidInput);
        }

        Ok(unsafe { Box::from_raw(r as *mut R) })
    }
}

impl<R> fmt::Debug for Thread<R> {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        formatter.debug_struct("Thread").field("tid", &self.tid).finish_non_exhaustive()
    }
}

extern "C" fn entry(arg: *mut libc::c_void) -> *mut libc::c_void {
    let ptr = arg as *mut Wrapper<()>;
    let closure = unsafe { Wrapper::from_raw(ptr) };
    closure.call() as *mut _
}

pub fn sleep(duration: Duration) -> Result<()> {
    let res = unsafe { libc::nanosleep(duration.as_raw(), ptr::null_mut()) };
    if res == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

pub fn yield_now() -> Result<()> {
    let res = unsafe { libc::sched_yield() };
    if res == -1 {
        Err(Error::last())
    } else {
        Ok(())
    }
}

pub fn spawn<F, R>(f: F) -> Result<Thread<R>>
where
    F: FnOnce() -> R,
    F: Send + 'static,
    R: Send + 'static,
{
    Thread::spawn(f)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::hint::black_box;

    #[test]
    fn spawn() {
        let t = Thread::spawn(move || black_box(42)).unwrap();
        assert_eq!(42, *t.join().unwrap());
    }
}

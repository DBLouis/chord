pub type Fd = libc::c_int;

use crate::{
    error::{Error, Result},
    fmt,
};

pub trait Read {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize>;
}

pub trait Write {
    fn write(&mut self, buf: &[u8]) -> Result<usize>;

    fn flush(&mut self) -> Result<()>;

    fn write_all(&mut self, mut buf: &[u8]) -> Result<()> {
        while !buf.is_empty() {
            match self.write(buf) {
                Ok(0) => return Err(Error::Io),
                Ok(n) => buf = &buf[n..],
                Err(Error::Intr) => continue,
                Err(e) => return Err(e),
            }
        }
        Ok(())
    }

    fn write_fmt(&mut self, fmt: fmt::Arguments<'_>) -> Result<()> {
        struct Adapter<'a, T: ?Sized> {
            inner: &'a mut T,
            error: Result<()>,
        }

        impl<T: Write + ?Sized> fmt::Write for Adapter<'_, T> {
            fn write_str(&mut self, s: &str) -> fmt::Result {
                match self.inner.write_all(s.as_bytes()) {
                    Ok(()) => Ok(()),
                    Err(e) => {
                        self.error = Err(e);
                        Err(fmt::Error)
                    }
                }
            }
        }

        let mut output = Adapter { inner: self, error: Ok(()) };
        match fmt::write(&mut output, fmt) {
            Ok(()) => Ok(()),
            Err(..) => {
                if output.error.is_err() {
                    output.error
                } else {
                    Err(Error::InvalidInput)
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct StdOut;

impl fmt::Write for StdOut {
    // FIXME broken when using redirect
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        unsafe { libc::printf(b"%.*s\0".as_ptr() as _, s.len() as i32, s.as_ptr()) };
        Ok(())
    }
}

#[derive(Debug)]
pub struct StdErr;

impl fmt::Write for StdErr {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        unsafe { libc::write(libc::STDERR_FILENO, s.as_ptr() as _, s.len() as _) };
        Ok(())
    }
}

#[derive(Debug)]
pub struct Void;

impl fmt::Write for Void {
    fn write_str(&mut self, _: &str) -> core::fmt::Result {
        Ok(())
    }
}

#[macro_export]
macro_rules! println {
    ($($arg:tt)*) => ({
        use $crate::fmt::Write;
        let _ = core::writeln!(&mut $crate::io::StdOut, $($arg)*);
    })
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ({
        use $crate::fmt::Write;
        let _ = core::write!(&mut $crate::io::StdOut, $($arg)*);
    })
}

#[macro_export]
macro_rules! eprintln {
    ($($arg:tt)*) => ({
        use $crate::fmt::Write;
        let _ = core::writeln!(&mut $crate::io::StdErr, $($arg)*);
    })
}

#[macro_export]
macro_rules! eprint {
    ($($arg:tt)*) => ({
        use $crate::fmt::Write;
        let _ = core::write!(&mut $crate::io::StdErr, $($arg)*);
    })
}

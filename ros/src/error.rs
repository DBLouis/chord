use crate::{ffi::CStr, fmt};
use libc::c_int;

pub type Result<T> = crate::result::Result<T, Error>;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Error {
    errno: i32,
}

impl From<i32> for Error {
    fn from(errno: i32) -> Self {
        Self { errno }
    }
}

impl Error {
    #![allow(non_upper_case_globals)]

    // TODO better names
    pub const AddrInUse: Self = Self::new(libc::EADDRINUSE);
    pub const Again: Self = Self::new(libc::EAGAIN);
    pub const ConnectionAborted: Self = Self::new(libc::ECONNABORTED);
    pub const ConnectionRefused: Self = Self::new(libc::ECONNREFUSED);
    pub const ConnectionReset: Self = Self::new(libc::ECONNRESET);
    pub const InProgress: Self = Self::new(libc::EINPROGRESS);
    pub const Intr: Self = Self::new(libc::EINTR);
    pub const InvalidInput: Self = Self::new(libc::EINVAL);
    pub const Io: Self = Self::new(libc::EIO);
    pub const Perm: Self = Self::new(libc::EPERM);
    pub const TimedOut: Self = Self::new(libc::ETIMEDOUT);

    /* Aliases */
    pub const WouldBlock: Self = Error::Again;

    pub fn last() -> Self {
        Error::from(errno_take())
    }

    const fn new(errno: i32) -> Self {
        Self { errno }
    }
}

#[cfg(target_os = "linux")]
unsafe fn errno_location() -> *mut c_int {
    libc::__errno_location()
}

#[cfg(target_os = "android")]
unsafe fn errno_location() -> *mut c_int {
    libc::__errno()
}

fn errno_clear() {
    unsafe { *errno_location() = 0 }
}

fn errno_read() -> i32 {
    unsafe { *errno_location() }
}

fn errno_take() -> i32 {
    let errno = errno_read();
    errno_clear();
    errno
}

impl fmt::Debug for Error {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, formatter)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = unsafe {
            let ptr = libc::strerror(self.errno);
            CStr::from_ptr(ptr)
        };
        let msg = msg.to_str().map_err(|_| fmt::Error)?;
        write!(formatter, "{}", msg)
    }
}

#[macro_export]
macro_rules! check {
    ($p:expr, $e:expr) => {
        if $p {
            Ok(())
        } else {
            Err($e)
        }
    };
}

#[macro_export]
macro_rules! check_eq {
    ($a:expr, $b:expr, $e:expr) => {
        $crate::check!($a == $b, $e)
    };
}

#[macro_export]
macro_rules! check_ne {
    ($a:expr, $b:expr, $e:expr) => {
        $crate::check!($a != $b, $e)
    };
}

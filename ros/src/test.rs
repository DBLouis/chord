use crate::{any, prelude::*};

pub fn runner(tests: &[&dyn Test]) {
    println!("Running {} tests", tests.len());
    for test in tests {
        test.run();
    }
}

pub trait Test {
    fn run(&self);
}

impl<T> Test for T
where
    T: Fn(),
{
    fn run(&self) {
        print!("[ ] {}\r", any::type_name::<T>());
        self();
        println!("[x] {}", any::type_name::<T>());
    }
}

#[test_case]
fn trivial() {
    assert_eq!(1, 1);
}

#[cfg(test)]
crate::main!(|_args: &[&str]| {
    crate::test_harness();
    Ok(())
});

pub fn abort() -> ! {
    unsafe { libc::abort() }
}

pub trait UnwrapExt<T> {
    fn unwrap_or_abort(self) -> T;
}

impl<T> UnwrapExt<T> for Option<T> {
    fn unwrap_or_abort(self) -> T {
        self.unwrap_or_else(|| abort())
    }
}

impl<T, E> UnwrapExt<T> for Result<T, E> {
    fn unwrap_or_abort(self) -> T {
        self.unwrap_or_else(|_| abort())
    }
}

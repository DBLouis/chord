# RustyChord - A Chord implementation in Rust

---

## Required nightly features

- [thread_local](https://github.com/rust-lang/rust/issues/29594)
- [ptr_metadata](https://github.com/rust-lang/rust/issues/81513)

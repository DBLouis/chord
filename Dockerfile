FROM scratch
ARG example=dummy
COPY /target/x86_64-unknown-linux-musl/release/examples/$example /bin/example
ENTRYPOINT [ "/bin/example" ]
